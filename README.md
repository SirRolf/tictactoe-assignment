# Tic tac toe
In this project I'm going to make the tic tac toe game. I'm focusing on both making it visually pleasing as well as implementing enough technical stuff.

# Class Diagram
![ClassDiagram](Documentation/ClassDiagram.png)

# Problems
Here are a few problems that i would like to fix if i put in more time

 * There could be a problem when you offset the max players and the size of the player sprite list in the Cellbehavior class.
 * I really wanted to make it so you could get 2 diffrent sizes. so it could be 7x4 for example. But i bumbed into problems with checking the diagonal win conditions. I do have another idea but i think it will take to much time. The idea was the following: Lets take an exemple of 7x4 as an example. How far the diagonal line can go is exactly the same length as the smallest size of the board. Knowing this we could make a simple for loop that would loop for the smallest size. In our case that would be 4 times. so from 0,0 to 4,4, then again for 0,4 to 4,4. The problem is, now we have to know how many times we have to do this... And this is w here i am stuck. So for now i'm making it so that you can only have a size that is the same on both the height and the width. But i kept in some code that would make it possible to still change it in the future that didn't clutter anything up.
 * The last cell you change before winning isn't as visable as the other cells. This is because it starts to dissapear the same frame it starts to appear.
 * Could have made one screen instead of the TieScreen and the WinScreen
 * In the CheckForVictory function i use to many bools. would like to do it with one but i don't know how