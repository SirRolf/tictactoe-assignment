using UnityEngine;

namespace FlorisProjecten.TTT.Bar
{
	/// <summary>
	/// Behavior that controls bars
	/// </summary>
	public class BarBehavior : MonoBehaviour
	{
		/// <summary>
		/// The width of the bars
		/// </summary>
		private const float BAR_WIDTH = .005f;
		/// <summary>
		/// The RectTransform used for changing sizes of this bar
		/// </summary>
		[SerializeField]
		private RectTransform BarImage;

		/// <summary>
		/// What type of bar this is.
		/// </summary>
		private BarLine barLine;

		/// <summary>
		/// Initialize function. You always need to call this before using <see cref="Open"/>
		/// </summary>
		/// <param name="position">The position for this bar</param>
		/// <param name="line">What type of bar this should be</param>
		public void Init(float position, BarLine line)
		{
			barLine = line;
			switch (barLine)
			{
				case BarLine.Horizontal:
					BarImage.anchorMin = new Vector2(0, position - BAR_WIDTH);
					BarImage.anchorMax = new Vector2(1, position + BAR_WIDTH);
					BarImage.localScale = new Vector3(0, 1, 1);
					break;
				case BarLine.Vertical:
					BarImage.anchorMin = new Vector2(position - BAR_WIDTH, 0);
					BarImage.anchorMax = new Vector2(position + BAR_WIDTH, 1);
					BarImage.localScale = new Vector3(1, 0, 1);
					break;
				default:
					break;
			}
		}
		/// <summary>
		/// Opens the bar so you can see it
		/// </summary>
		public void Open()
		{
			LeanTween.scale(BarImage, Vector3.one, 1.5f)
				.setEase(LeanTweenType.easeInCubic);
		}
		/// <summary>
		/// Closes the bar so it can be removed
		/// </summary>
		public void Close()
		{
			if (barLine == BarLine.Horizontal)
			{
				LeanTween.scale(BarImage, new Vector3(0, 1, 1), 1f)
					.setEase(LeanTweenType.easeOutCubic);
			}
			else
			{
				LeanTween.scale(BarImage, new Vector3(1, 0, 1), 1f)
					.setEase(LeanTweenType.easeOutCubic);
			}
		}
	}
}
