using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FlorisProjecten.TTT.Cell
{
	/// <summary>
	/// Behavior for the cells
	/// </summary>
	public class CellBehavior : MonoBehaviour
	{
		/// <summary>
		/// Transform of this cell
		/// </summary>
		[SerializeField]
		private RectTransform myTransform;
		/// <summary>
		/// Button attached to this cell
		/// </summary>
		[SerializeField]
		private Button cellButton;
		/// <summary>
		/// Image attached to the cell representing which player is owner of this cell
		/// </summary>
		[SerializeField]
		private Image playerImage;
		/// <summary>
		/// the sprites for the players
		/// </summary>
		[SerializeField]
		private List<Sprite> playerSprites;
		/// <summary>
		/// Canvas group attached to the Image, Used for having a nice animation when you change one
		/// </summary>
		[SerializeField]
		private CanvasGroup playerImageCanvasGroup;
		/// <summary>
		/// What player is the current owner of this cell
		/// </summary>
		private int currentOwner;
		/// <summary>
		/// What player is the current owner of this cell
		/// </summary>
		public int CurrentOwner => currentOwner;
		/// <summary>
		/// Initialize function. Set's up location and adds listener to the button
		/// </summary>
		/// <param name="OnCellPressed">Get's called whenever the button is pressed</param>
		/// <param name="cellLocation">What location this cell is in</param>
		/// <param name="boardSize">The size ofthe board used for placing the cell</param>
		public void Init(Action<Vector2> OnCellPressed, Vector2 cellLocation, Vector2 boardSize)
		{
			myTransform.anchorMin = new Vector2(cellLocation.x / boardSize.x, cellLocation.y / boardSize.y);
			myTransform.anchorMax = new Vector2((cellLocation.x + 1) / boardSize.x, (cellLocation.y + 1) / boardSize.y);
			cellButton.onClick.AddListener(() =>
			{
				OnCellPressed(cellLocation);
				cellButton.onClick.RemoveAllListeners();
			});
		}
		/// <summary>
		/// Changes the image attached to the cell
		/// </summary>
		/// <param name="playerNumber">What player should be the owner of this cell</param>
		public void ChangePlayerImage(int playerNumber)
		{
			LeanTween.alphaCanvas(playerImageCanvasGroup, 1f, 1f)
				.setEase(LeanTweenType.easeOutCirc);
			playerImage.sprite = playerSprites[playerNumber - 1];
			currentOwner = playerNumber;
		}

		/// <summary>
		/// Hides this cell
		/// </summary>
		public void HidePlayerImage()
		{
			cellButton.onClick.RemoveAllListeners();
			LeanTween.alphaCanvas(playerImageCanvasGroup, 0f, 1f)
				.setEase(LeanTweenType.easeOutCubic);
		}
	}
}
