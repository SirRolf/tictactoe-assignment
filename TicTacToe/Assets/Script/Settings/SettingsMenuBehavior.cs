using FlorisProjecten.TTT.Board;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace FlorisProjecten.TTT.Settings
{
	public class SettingsMenuBehavior : MonoBehaviour
	{
		[SerializeField]
		private Button settingsButton;
		[SerializeField]
		private Button closeSettingsButton;
		/// <summary>
		/// The rect transform of the settings Button
		/// </summary>
		[SerializeField]
		private RectTransform settingsButtonTransform;
		/// <summary>
		/// The rect transform of the settings menu
		/// </summary>
		[SerializeField]
		private RectTransform settingsMenu;
		/// <summary>
		/// Dropdown for inputting the size of the board
		/// </summary>
		[SerializeField]
		private TMP_Dropdown sizeDropdown;
		/// <summary>
		/// Dropdown for inputting the ammount of players
		/// </summary>
		[SerializeField]
		private TMP_Dropdown playerDropdown;
		/// <summary>
		/// The board manager. Used for making a new board
		/// </summary>
		[SerializeField]
		private BoardManager boardManager;

		private Vector3 settingsMenuHiddenPosition;

		private void Start()
		{
			settingsMenuHiddenPosition = new Vector3(settingsMenu.localPosition.x + settingsMenu.rect.width, settingsMenu.localPosition.y - settingsMenu.rect.height);
			settingsButtonTransform.localPosition = new Vector3(settingsButtonTransform.localPosition.x + (Screen.width * .3f), settingsButtonTransform.localPosition.y - (Screen.height * .3f));
			LeanTween.move(settingsButtonTransform, Vector3.zero, 2f)
				.setEase(LeanTweenType.easeOutBack);
			Debug.Log(settingsMenuHiddenPosition);
			settingsMenu.localPosition = settingsMenuHiddenPosition;
			closeSettingsButton.onClick.AddListener(Close);
			settingsButton.onClick.AddListener(Open);
		}

		private void Open()
		{
			LeanTween.move(settingsButtonTransform, new Vector3(Screen.width * .2f, -Screen.height * .2f), .5f)
				.setEase(LeanTweenType.easeInBack)
				.setOnComplete(() =>
				{
					LeanTween.move(settingsMenu, Vector3.zero, 1f)
						.setEase(LeanTweenType.easeOutCubic);
					LeanTween.move(settingsButtonTransform, Vector3.zero, 1f);
				});
		}

		private void Close()
		{
			LeanTween.move(settingsMenu, settingsMenuHiddenPosition, 1f)
				.setEase(LeanTweenType.easeInCubic);
			boardManager.CreateBoard(CreateBoardVariables());
		}

		private BoardVariables CreateBoardVariables()
		{
			int boardSize = sizeDropdown.value + 2;
			int playerAmount = playerDropdown.value + 2;
			return new BoardVariables(boardSize, playerAmount);
		}
	}
}
