using UnityEngine;

namespace FlorisProjecten.TTT.Board
{
	/// <summary>
	/// Class containing all the variables for making a new board
	/// </summary>
	public class BoardVariables
	{
		/// <summary>
		/// The maximum amount of players. Has to do with the amount of player sprites that are in the cellprefab
		/// </summary>
		private const int MAX_PLAYERS = 4;

		/// <summary>
		/// Size of the board
		/// </summary>
		private Vector2 size;
		/// <summary>
		/// Size of the board
		/// </summary>
		public Vector2 Size => size;
		/// <summary>
		/// Amount of player playing
		/// </summary>
		private int playerAmount;
		/// <summary>
		/// Amount of player playing
		/// </summary>
		public int PlayerAmount => playerAmount;

		/// <summary>
		/// Class containing all the variables for making a new board
		/// </summary>
		/// <param name="size">Size of the board</param>
		/// <param name="playerAmount">Amount of player playing</param>
		public BoardVariables(int size = 3, int playerAmount = 2)
		{
			this.size = new Vector2(size, size);
			if (playerAmount > MAX_PLAYERS)
				playerAmount = MAX_PLAYERS;
			this.playerAmount = playerAmount;
		}
	}
}
