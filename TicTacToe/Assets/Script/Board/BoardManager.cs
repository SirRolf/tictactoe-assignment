using FlorisProjecten.TTT.Bar;
using FlorisProjecten.TTT.Cell;
using System.Collections.Generic;
using UnityEngine;

namespace FlorisProjecten.TTT.Board
{
	/// <summary>
	/// Manages multiple aspects about the board
	/// </summary>
	public class BoardManager : MonoBehaviour
	{
		/// <summary>
		/// This is going to be the parent of the board
		/// </summary>
		[SerializeField]
		private Transform BoardLocation;
		/// <summary>
		/// The screen you see when you win
		/// </summary>
		[Header("Screens")]
		[SerializeField]
		private RectTransform winScreen;
		/// <summary>
		/// The screen you see when you win
		/// </summary>
		[SerializeField]
		private RectTransform tieScreen;

		/// <summary>
		/// Prefab for the board
		/// </summary>
		[Header("Prefabs")]
		[SerializeField]
		private RectTransform boardPrefab;
		/// <summary>
		/// Prefab for the bars
		/// </summary>
		[SerializeField]
		private BarBehavior barPrefab;
		/// <summary>
		/// Prefab for the cells
		/// </summary>
		[SerializeField]
		private CellBehavior cellPrefab;

		/// <summary>
		/// The current board
		/// </summary>
		private RectTransform board;
		/// <summary>
		/// The variables of the current board
		/// </summary>
		private BoardVariables currentBoardVariables;
		/// <summary>
		/// List containing all the bars that have been made
		/// </summary>
		private List<BarBehavior> bars;
		/// <summary>
		/// The diffrent cells
		/// </summary>
		private CellBehavior[,] cells;
		/// <summary>
		/// The player that can place change it's cell at this moment
		/// </summary>
		private int currentPlayer = 1;
		/// <summary>
		/// Checks what round it is so you can stop the game once it is a tie
		/// </summary>
		private int roundChecker = 1;

		/// <summary>
		/// Just creates the first board
		/// </summary>
		private void Start()=> CreateBoard(new BoardVariables());

		/// <summary>
		/// Creates a new board with cells and bars
		/// </summary>
		/// <param name="variables">The boardvariables containing all the variables needed for making a new board</param>
		public void CreateBoard(BoardVariables variables)
		{
			if (board != null)
				Destroy(board.gameObject);
			currentBoardVariables = variables;
			board = Instantiate(boardPrefab, BoardLocation);
			cells = new CellBehavior[(int)variables.Size.x, (int)variables.Size.y];
			bars = new List<BarBehavior>();
			winScreen.position = new Vector3(Screen.width / 2, 0);
			tieScreen.position = new Vector3(Screen.width / 2, 0);
			for (int i = 1; i < variables.Size.x; i++)
			{
				BarBehavior newBar = Instantiate(barPrefab, board);
				newBar.Init(i / variables.Size.x, BarLine.Horizontal);
				bars.Add(newBar);
				newBar.Open();
			}
			for (int i = 1; i < variables.Size.y; i++)
			{
				BarBehavior newBar = Instantiate(barPrefab, board);
				newBar.Init(i / variables.Size.y, BarLine.Vertical);
				bars.Add(newBar);
				newBar.Open();
			}
			for (int i = 0; i < variables.Size.x; i++)
			{
				for (int j = 0; j < variables.Size.y; j++)
				{
					CellBehavior cell = Instantiate(cellPrefab, board);
					cell.Init(CheckForVictory, new Vector2(i, j), variables.Size);
					cells[i, j] = cell;
				}
			}
		}
		/// <summary>
		/// Checks if a player has won and changes the cell image
		/// </summary>
		/// <param name="changedCellLocation">What cell has been changed</param>
		private void CheckForVictory(Vector2 changedCellLocation)
		{
			cells[(int)changedCellLocation.x, (int)changedCellLocation.y].ChangePlayerImage(currentPlayer);
			bool playerHasWonHorizontal = true;
			bool playerHasWonVertical = true;
			bool playerHasWonDiagonallyRightUp = true;
			bool playerHasWonDiagonallyLeftUp = true;
			bool isTie = false;
			RectTransform screenType = winScreen;
			for (int i = 0; i < cells.GetLength(0); i++)
			{
				if (cells[i, (int)changedCellLocation.y].CurrentOwner != currentPlayer)
				{
					playerHasWonHorizontal = false;
					break;
				}
			}
			for (int i = 0; i < cells.GetLength(1); i++)
			{
				if (cells[(int)changedCellLocation.x, i].CurrentOwner != currentPlayer)
				{
					playerHasWonVertical = false;
					break;
				}
			}
			for (int i = 0; i < currentBoardVariables.Size.x; i++)
			{
				if (cells[i, i].CurrentOwner != currentPlayer)
				{
					playerHasWonDiagonallyRightUp = false;
					break;
				}
			}
			for (int i = 0; i < currentBoardVariables.Size.y; i++)
			{
				if (cells[i, ((int)currentBoardVariables.Size.y - 1) - i].CurrentOwner != currentPlayer)
				{
					playerHasWonDiagonallyLeftUp = false;
					break;
				}
			}
			currentPlayer++;
			if (currentPlayer > currentBoardVariables.PlayerAmount)
				currentPlayer = 1;
			if (roundChecker == currentBoardVariables.Size.x * currentBoardVariables.Size.y && !playerHasWonHorizontal && !playerHasWonVertical && !playerHasWonDiagonallyRightUp && !playerHasWonDiagonallyLeftUp)
			{
				isTie = true;
				screenType = tieScreen;
			}
			roundChecker++;
			if (playerHasWonHorizontal || playerHasWonVertical || playerHasWonDiagonallyRightUp || playerHasWonDiagonallyLeftUp || isTie)
			{
				roundChecker = 1;
				HideBoard();
				LeanTween.move(screenType, new Vector3(), 1.5f)
					.setEase(LeanTweenType.easeOutQuint)
					.setOnComplete(() =>
					{
						LeanTween.move(screenType, new Vector3(0, Screen.height * 2), 1.5f)
							.setEase(LeanTweenType.easeInQuint)
							.setOnComplete(() => CreateBoard(currentBoardVariables));
					});
			}
		}

		public void HideBoard()
		{
			for (int i = 0; i < bars.Count; i++)
				bars[i].Close();
			foreach (CellBehavior cell in cells)
				cell.HidePlayerImage();
		}
	}
}
